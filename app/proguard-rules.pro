-keepattributes SourceFile,LineNumberTable
-keepattributes LocalVariableTable, LocalVariableTypeTable
-renamesourcefileattribute SourceFile
-keepattributes *Annotation*, Signature, Exception

-keepclasseswithmembernames class * { native <methods>; }
-keep class com.example.iconfinder.model.IconModel** { *; }