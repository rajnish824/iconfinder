package com.example.iconfinder

import android.app.Application
import com.example.iconfinder.di.networkModule
import org.koin.android.ext.android.startKoin

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(networkModule), loadPropertiesFromFile = true)
    }
}