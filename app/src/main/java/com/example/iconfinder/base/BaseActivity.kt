package com.example.iconfinder.base

import android.os.Bundle
import android.widget.Toast
import android.widget.Toolbar
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.example.iconfinder.R
import com.example.iconfinder.model.ErrorResponse

abstract class BaseActivity : AppCompatActivity(), BaseErrorInterface {

    @LayoutRes
    protected abstract fun layoutRes(): Int
    protected open fun init(){}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes())
        init()
    }

    override fun onBadRequest() {
        super.onBadRequest()
        Toast.makeText(this, R.string.bad_request, Toast.LENGTH_SHORT).show()
    }

    override fun onForbidden() {
        super.onForbidden()
        Toast.makeText(this, R.string.forbidden, Toast.LENGTH_SHORT).show()
    }

    override fun onNotFound() {
        super.onNotFound()
        Toast.makeText(this, R.string.not_found, Toast.LENGTH_SHORT).show()
    }

    override fun onTooManyRequest() {
        super.onTooManyRequest()
        Toast.makeText(this, R.string.too_many_request, Toast.LENGTH_SHORT).show()
    }

    override fun onUnAuthorizedAccess() {
        super.onUnAuthorizedAccess()
        Toast.makeText(this, R.string.un_authorised, Toast.LENGTH_SHORT).show()
    }

    override fun showOtherError(errorResponse: ErrorResponse) {
        Toast.makeText(this, errorResponse.toString(), Toast.LENGTH_SHORT).show()
    }

    override fun showInternetError() {
        super.showInternetError()
        Toast.makeText(this, R.string.internet_error, Toast.LENGTH_SHORT).show()
    }
}
