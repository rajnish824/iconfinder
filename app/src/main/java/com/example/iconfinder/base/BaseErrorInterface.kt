package com.example.iconfinder.base

import com.example.iconfinder.model.ErrorResponse
import com.google.gson.Gson
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException


interface BaseErrorInterface {
    fun showError(e: Exception) {
        when (e) {
            is HttpException -> {
                val response = e.response()

                val errorResponse =
                    Gson().fromJson(response?.errorBody()!!.toString(), ErrorResponse::class.java)
                when (response.code()) {
                    400 -> onBadRequest()
                    401 -> onUnAuthorizedAccess()
                    403 -> onForbidden()
                    404 -> onNotFound()
                    429 -> onTooManyRequest()
                    else -> showOtherError(errorResponse)

                }
            }

            is SocketTimeoutException -> showSocketTimeoutError()
            is IOException -> showInternetError()
        }
    }

    fun showInternetError() {
        showOtherError(ErrorResponse())
    }

    fun showSocketTimeoutError() {
        showOtherError(ErrorResponse())
    }


    fun onTooManyRequest() {
        showOtherError(ErrorResponse())
    }

    fun onNotFound() {
        showOtherError(ErrorResponse())
    }

    fun onForbidden() {
        showOtherError(ErrorResponse())
    }

    fun onUnAuthorizedAccess() {
        showOtherError(ErrorResponse())
    }

    fun onBadRequest() {
        showOtherError(ErrorResponse())
    }

    fun showOtherError(errorResponse: ErrorResponse)
}