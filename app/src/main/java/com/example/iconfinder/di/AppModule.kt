package com.example.iconfinder.di

import com.example.iconfinder.repository.IconRepository
import com.example.iconfinder.base.BaseErrorInterface
import com.example.iconfinder.retrofit.createIconService
import com.example.iconfinder.viewModel.IconViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val networkModule = module {
    single { createIconService() }
    single { IconRepository(get()) }
    viewModel { IconViewModel(get()) }
}