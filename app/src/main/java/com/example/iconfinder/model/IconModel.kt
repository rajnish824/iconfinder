package com.example.iconfinder.model


import com.google.gson.annotations.SerializedName

data class IconModel(
    @SerializedName("icons")
    val icons: List<Icon>,
    @SerializedName("total_count")
    val totalCount: Int
) {
    data class Icon(
        @SerializedName("categories")
        val categories: List<Category>,
        @SerializedName("containers")
        val containers: List<Container>,
        @SerializedName("icon_id")
        val iconId: Int,
        @SerializedName("is_icon_glyph")
        val isIconGlyph: Boolean,
        @SerializedName("is_premium")
        val isPremium: Boolean,
        @SerializedName("is_purchased")
        val isPurchased: Boolean,
        @SerializedName("prices")
        val prices: List<Price>,
        @SerializedName("published_at")
        val publishedAt: String,
        @SerializedName("raster_sizes")
        val rasterSizes: List<RasterSize>,
        @SerializedName("styles")
        val styles: List<Style>,
        @SerializedName("tags")
        val tags: List<String>,
        @SerializedName("type")
        val type: String,
        @SerializedName("vector_sizes")
        val vectorSizes: List<VectorSize>
    ) {
        data class Category(
            @SerializedName("identifier")
            val identifier: String,
            @SerializedName("name")
            val name: String
        )

        data class Container(
            @SerializedName("download_url")
            val downloadUrl: String,
            @SerializedName("format")
            val format: String
        )

        data class Price(
            @SerializedName("currency")
            val currency: String,
            @SerializedName("license")
            val license: License,
            @SerializedName("price")
            val price: Double
        ) {
            data class License(
                @SerializedName("license_id")
                val licenseId: Int,
                @SerializedName("name")
                val name: String,
                @SerializedName("scope")
                val scope: String,
                @SerializedName("url")
                val url: String
            )
        }

        data class RasterSize(
            @SerializedName("formats")
            val formats: List<Format>,
            @SerializedName("size")
            val size: Int,
            @SerializedName("size_height")
            val sizeHeight: Int,
            @SerializedName("size_width")
            val sizeWidth: Int
        ) {
            data class Format(
                @SerializedName("download_url")
                val downloadUrl: String,
                @SerializedName("format")
                val format: String,
                @SerializedName("preview_url")
                val previewUrl: String
            )
        }

        data class Style(
            @SerializedName("identifier")
            val identifier: String,
            @SerializedName("name")
            val name: String
        )

        data class VectorSize(
            @SerializedName("formats")
            val formats: List<Format>,
            @SerializedName("size")
            val size: Int,
            @SerializedName("size_height")
            val sizeHeight: Int,
            @SerializedName("size_width")
            val sizeWidth: Int,
            @SerializedName("target_sizes")
            val targetSizes: List<List<Int>>
        ) {
            data class Format(
                @SerializedName("download_url")
                val downloadUrl: String,
                @SerializedName("format")
                val format: VectorFormat
            )
        }
        enum class VectorFormat{
            @SerializedName("png")
            PNG,
            @SerializedName("svg")
            SVG,
            @SerializedName("ai")
            AI,
            @SerializedName("csh")
            CSH
        }
    }
}