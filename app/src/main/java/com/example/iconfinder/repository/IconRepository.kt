package com.example.iconfinder.repository

import com.example.iconfinder.retrofit.ApiService

class IconRepository(private val apiService: ApiService) {
    suspend fun getIcons(query: String, count: Int) = apiService.getIcons(
        query, count,
    )
}