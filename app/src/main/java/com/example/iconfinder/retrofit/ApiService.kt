package com.example.iconfinder.retrofit

import com.example.iconfinder.model.IconModel
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
//    @Headers("Authorization: Bearer 6oT5CLBz32ANLUyubau9McDSHTNgAdxxM61BV6EOynJFH0f2vlMHhD8v66I5UfBZ")

    @GET("search")
    suspend fun getIcons(
        @Query("query") query: String,
        @Query("count") count: Int,
    ): IconModel
}