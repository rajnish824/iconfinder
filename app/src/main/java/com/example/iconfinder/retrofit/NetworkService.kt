package com.example.iconfinder.retrofit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

//private fun httpInterceptor() = HttpLoggingInterceptor().apply {
//    level = HttpLoggingInterceptor.Level.BODY
//}
//
//private fun basicOkHttpClient() = OkHttpClient.Builder().addInterceptor(httpInterceptor()).build()

private fun basicOkHttpClient() = OkHttpClient.Builder().apply {
    addInterceptor(Interceptor { chain ->
        val builder = chain.request().newBuilder()
            .addHeader(
                "Authorization",
                "Bearer 6oT5CLBz32ANLUyubau9McDSHTNgAdxxM61BV6EOynJFH0f2vlMHhD8v66I5UfBZ"
            )
        return@Interceptor chain.proceed((builder.build()))
    })
}.build()


fun createIconService(): ApiService {
    val baseUrl = "https://iconfinder-api-auth.herokuapp.com/v4/icons/"
    val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create()).client(basicOkHttpClient())
        .baseUrl(baseUrl)
        .build()
    return retrofit.create(ApiService::class.java)
}