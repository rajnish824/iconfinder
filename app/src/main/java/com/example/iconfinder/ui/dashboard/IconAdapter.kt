package com.example.iconfinder.ui.dashboard

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.iconfinder.R
import com.example.iconfinder.model.IconModel
import com.example.iconfinder.utils.hide

class IconAdapter constructor(
    private var iconList: List<IconModel.Icon>,
    val listener: OnItemCLickListener
) :
    RecyclerView.Adapter<IconAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_icon_holder, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(holder.itemView.context)
            .load(iconList[position].rasterSizes.sortedBy { it.size }
                .last().formats.first().previewUrl)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    holder.progressBar.hide()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    holder.progressBar.hide()
                    return false
                }
            })
            .into(holder.image)

        if (iconList[position].isPremium)
            Glide.with(holder.itemView.context).load(R.drawable.premium).into(holder.isPremium)
    }

    override fun getItemCount(): Int {
        return iconList.count()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val image: ImageView = itemView.findViewById(R.id.icon)
        var progressBar: ProgressBar = itemView.findViewById(R.id.progress_bar)
        val isPremium: ImageView = itemView.findViewById(R.id.is_premium)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = absoluteAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemCLickListener {
        fun onItemClick(position: Int)
    }
}
