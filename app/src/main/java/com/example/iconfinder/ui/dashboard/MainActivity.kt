package com.example.iconfinder.ui.dashboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.iconfinder.R
import com.example.iconfinder.base.BaseActivity
import com.example.iconfinder.databinding.ActivityMainBinding
import com.example.iconfinder.model.IconModel
import com.example.iconfinder.ui.secondPage.SecondActivity
import com.example.iconfinder.utils.hide
import com.example.iconfinder.utils.show
import com.example.iconfinder.viewModel.IconViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity(), IconAdapter.OnItemCLickListener {
    private val iconViewModel: IconViewModel by viewModel()

    private lateinit var iconAdapter: IconAdapter
    private lateinit var allData: List<IconModel.Icon>
    private lateinit var imageList: ArrayList<String>
    private var lastSearchQuery: String = ""
    private var countValue: Int = 30

    private lateinit var binding: ActivityMainBinding
    override fun layoutRes(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initRecyclerView()
        initSearch()
    }

    private fun initRecyclerView() {
        val layoutManager = GridLayoutManager(this, 2)
        binding.recyclerView.layoutManager = layoutManager
        getIcons()

        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    countValue += 30
                    refresh()
                }
            }

        })
        binding.swipeToRefresh.setOnRefreshListener {
            refresh()
            Handler(Looper.getMainLooper()).postDelayed({
                binding.swipeToRefresh.isRefreshing = false
            }, 2000)
        }
    }

    private fun getIcons() {
        binding.progressBar.show()
        iconViewModel.allData.observe(
            this,
            { iconList ->
                iconList?.let {
                    allData = iconList
                    iconAdapter = IconAdapter(iconList, this)
                    binding.recyclerView.adapter = iconAdapter
                    iconAdapter.notifyItemRangeInserted(countValue, iconList.size - 1)
                    binding.progressBar.hide()
                }
            })
        iconViewModel.getAllIcons()
    }
    private fun refresh() = iconViewModel.getAllIcons(query = lastSearchQuery, count = countValue)


    override fun onItemClick(position: Int) {
        val noOfImages = allData[position].rasterSizes.size
        imageList = ArrayList(noOfImages)
        for (i in 0 until noOfImages) {
            imageList.add(allData[position].rasterSizes[i].formats.first().previewUrl)
        }

        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("url", imageList)
        startActivity(intent)
    }

    private fun initSearch() {
        binding.searchBtn.setOnClickListener {
            val query = binding.search.text.toString()
            binding.search.hideKeyboard()
            iconViewModel.getAllIcons(query = query)
            lastSearchQuery = query
        }
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }


}