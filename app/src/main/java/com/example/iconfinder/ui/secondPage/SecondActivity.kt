package com.example.iconfinder.ui.secondPage

import android.app.AlertDialog
import android.app.DownloadManager
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.iconfinder.R
import com.example.iconfinder.base.BaseActivity
import com.example.iconfinder.databinding.ActivitySecondBinding
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import java.io.File

class SecondActivity : BaseActivity(), ImageAdapter.OnImageClickListener {
    private lateinit var imageAdapter: ImageAdapter
    private var imageList: ArrayList<String>? = null
    private var message: String? = ""
    private var lastMsg = ""

    private lateinit var binding: ActivitySecondBinding

    override fun layoutRes(): Int {
        return R.layout.activity_second
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySecondBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbar2)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        imageList = intent.getStringArrayListExtra("url")

        initRecyclerView()
    }

    private fun initRecyclerView() {
        val layoutManager = GridLayoutManager(this, 2)
        binding.recyclerView.layoutManager = layoutManager
        imageAdapter = imageList?.let { ImageAdapter(it, this) }!!
        imageAdapter.notifyDataSetChanged()
        binding.recyclerView.adapter = imageAdapter
    }

    override fun onItemClick(position: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            checkPermission(imageList?.get(position))
        } else {
            showAlertBeforeDownload(imageList?.get(position))
        }
//        val url = imageList?.get(position)
//        val intent = Intent(Intent.ACTION_VIEW)
//        intent.data = Uri.parse(url)
//        startActivity(intent)

    }

    private fun downloadImage(url: String?) {
        val directory = File(Environment.DIRECTORY_PICTURES)

        if (!directory.exists()) {
            directory.mkdirs()
        }

        val downloadManager = this.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val downloadUri = Uri.parse(url)

        val request = DownloadManager.Request(downloadUri).apply {
            setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(url?.substring(url.lastIndexOf("/") + 1))
                .setDescription("")
                .setDestinationInExternalPublicDir(
                    directory.toString(),
                    url?.substring(url.lastIndexOf("/") + 1)
                )
        }

        val downloadId = downloadManager.enqueue(request)
        val query = DownloadManager.Query().setFilterById(downloadId)
        Thread {
            var downloading = true
            while (downloading) {
                val cursor: Cursor = downloadManager.query(query)
                cursor.moveToFirst()
                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                    downloading = false
                }
                val status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                message = statusMessage(url, directory, status)
                if (message != lastMsg) {
                    this.runOnUiThread {
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                    }
                    lastMsg = message ?: ""
                }
                cursor.close()
            }
        }.start()
    }

    private fun statusMessage(url: String?, directory: File, status: Int): String {
        return when (status) {
            DownloadManager.STATUS_FAILED -> "Download has been failed, please try again"
            DownloadManager.STATUS_PAUSED -> "Paused"
            DownloadManager.STATUS_PENDING -> "Pending"
            DownloadManager.STATUS_RUNNING -> "Downloading..."
            DownloadManager.STATUS_SUCCESSFUL -> "Image downloaded successfully in $directory" + File.separator + url?.substring(
                url.lastIndexOf("/") + 1
            )
            else -> "There's nothing to download"
        }
    }

    private fun checkPermission(url: String?) {
        Dexter.withContext(this@SecondActivity)
            .withPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    showAlertBeforeDownload(url)
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    //denied
                    Toast.makeText(
                        this@SecondActivity,
                        R.string.permission_denied,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }
            })
            .withErrorListener {
                Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_SHORT).show()
            }
    }

    private fun showAlertBeforeDownload(url: String?) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.download_permission)
        builder.setIcon(R.drawable.download)
        builder.setPositiveButton(R.string.yes) { _, _ ->
            downloadImage(url)
        }
        builder.setNegativeButton(R.string.no) { _, _ ->
            //negative
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(true)
        alertDialog.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}