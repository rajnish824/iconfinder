package com.example.iconfinder.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.iconfinder.base.BaseErrorInterface
import com.example.iconfinder.model.IconModel
import com.example.iconfinder.repository.IconRepository
import kotlinx.coroutines.launch
import org.koin.standalone.KoinComponent

class IconViewModel(private val iconRepository: IconRepository) : ViewModel(), KoinComponent {

    private var listOfIcons = MutableLiveData<List<IconModel.Icon>>()
    var allData: LiveData<List<IconModel.Icon>> = listOfIcons

    fun getAllIcons(query: String = "", count: Int = 30) {
        viewModelScope.launch {
            try {
                val response = iconRepository.getIcons(query, count)
                listOfIcons.postValue(response.icons)
                Log.d("Repo", response.totalCount.toString())
            } catch (e: Exception) {
                Log.e("Error", e.stackTraceToString())
            }
        }
    }
}